require 'rails_helper'

RSpec.describe Booking, type: :model do
  it{should have_db_column(:check_in)}
  it{should have_db_column(:check_out)}
  it{should have_db_column(:cooking_assistance)}
  it{should have_db_column(:reference_number)}
end
