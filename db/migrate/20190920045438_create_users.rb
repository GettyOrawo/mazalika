class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.integer :contact
      t.belongs_to :booking, index: { unique: true }, foreign_key: true
      
      t.timestamps
    end
  end
end
