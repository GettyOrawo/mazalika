class CreateBookings < ActiveRecord::Migration[6.0]
  def change
    create_table :bookings do |t|
      t.datetime :check_in
      t.datetime :check_out
      t.boolean :cooking_assistance
      t.integer :reference_number

      t.timestamps
    end
  end
end
